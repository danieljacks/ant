using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
/// Base class for an ant
public class AntM : MonoBehaviour {
    public NavMeshAgent agent;

    void Start () {
        agent = GetComponent<NavMeshAgent>();
        MoveToPosition (new Vector3 (UnityEngine.Random.Range(-10.0f, 10.0f),0 ,UnityEngine.Random.Range(-10.0f, 10.0f)));
    }

    public void MoveToPosition (Vector3 position) {
        agent.destination = position;
        agent.isStopped = false;
    }

    public void ON_A_BLOCK_ADDED_OR_REMOVED () {
        // make this update on an event when a block is changed, and it should recalculate the navMeshAgent path
    }

    

}