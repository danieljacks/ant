﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/// Dirt objects are assumed to be stationary and never moved. If they need to, destroy them and reinstantiate as something else (a moving version which is off the grid, say)
public class DirtManager : Singleton<DirtManager> {

	public int gridWidth = 10;
	public int gridHeight = 10;
	public float gridCellSize = 1.0f;
	public GameObject dirtBasicPrefab;
	public GameObject workerAntPrefab;

	public NavMeshSurface dirtNavSurface;

	private DirtType[,] dirtGrid;
	/// bottom left is 0,0 and coordinates are positive.
	
	private GameObject[,] dirtObjects;

	void Start () {
		dirtGrid = new DirtType[gridWidth, gridHeight];
		dirtObjects = new GameObject[gridWidth, gridHeight];

		GenerateDirt();
		UpdateNavSurface();
	}


	private void GenerateDirt () {
		for (int x = 0; x < gridWidth; x++) {
			for (int y = 0; y < gridHeight; y++) {
				SetDirtGrid(new Vector2Int(x, y), DirtType.Basic);
			}
		}
	}
	private void GenerateAnts () {
		int x = UnityEngine.Random.Range (0, gridWidth);
		int y = UnityEngine.Random.Range (0, gridHeight);
		Vector2Int gridPos = new Vector2Int(x, y);

		SetDirtGrid (gridPos, DirtType.Empty);
		Instantiate (workerAntPrefab, GridToWorldPos(gridPos, 0.25f), Quaternion.identity);
	}

	private void UpdateNavSurface () {
		dirtNavSurface.BuildNavMesh ();
	}

	public void SetDirtGrid (Vector2Int pos, DirtType type) {
		dirtGrid[pos.x, pos.y] = type;
		SetDirtObject (pos, type);
	}
	public DirtType GetDirtGrid (Vector2Int pos) {
		return dirtGrid[pos.x, pos.y];
	}

	public void SetDirtObject (Vector2Int pos, DirtType type) {
		switch (type) {
			case DirtType.Empty:
				RemoveDirtObject (pos);
				break;
			case DirtType.Basic:
				SetDirtObject (pos, dirtBasicPrefab);
				break;
			default:
				Debug.LogError ("unknown value");
				break;
		}
	}
	public GameObject GetDirtObect (Vector2Int pos) {
		return dirtObjects[pos.x, pos.y];
	}

	private void SetDirtObject (Vector2Int pos, GameObject prefab) {
		GameObject oldObject = dirtObjects[pos.x, pos.y];

		DirtType requiredType 	= prefab.GetComponent<DirtM>().DirtType;
		DirtType oldType 		= oldObject == null ? DirtType.Empty : oldObject.GetComponent<DirtM>().DirtType;

		if (oldType != requiredType) {
			Destroy (oldObject);
			GameObject newGO = Instantiate (prefab, GridToWorldPos(pos, 0.5f), Quaternion.identity) as GameObject;
			dirtObjects[pos.x, pos.y] = newGO;
			dirtNavSurface.BuildNavMesh();
			newGO.GetComponent<DirtM>().gridPos = pos;
		}
	}
	private void RemoveDirtObject (Vector2Int pos) {
		if (dirtObjects[pos.x, pos.y] != null) {
			Destroy (dirtObjects[pos.x, pos.y]);
			dirtObjects[pos.x, pos.y] = null;
		}
	}

	public Vector3 GridToWorldPos (Vector2Int gridPos, float worldHeight) {
		Vector3 worldPos = new Vector3 (gridPos.x - (gridWidth / 2), worldHeight, gridPos.y - (gridHeight / 2));
		worldPos *= gridCellSize;
		return worldPos;
	}

	public Vector2Int WorldToGridPos (Vector3 worldPos) {
		int x = Mathf.RoundToInt((worldPos.x + (gridWidth / 2)) / gridCellSize);
		int y = Mathf.RoundToInt((worldPos.z + (gridHeight / 2)) / gridCellSize);
		Vector2Int gridPos = new Vector2Int (x, y);

		return gridPos;
	}

	public void Update () {
		if (Input.GetMouseButtonDown (0)) {
			Vector3 hitPoint;
			if (RaycastMouseToGround(out hitPoint)) {
				Vector2Int gridPos = WorldToGridPos (hitPoint);
				if (CheckInGridBounds(gridPos)) {
					if (GetDirtGrid (gridPos) == DirtType.Empty) {
						SetDirtGrid (gridPos, DirtType.Basic);
					} else {
						SetDirtGrid (gridPos, DirtType.Empty);
					}
				}
			}
		}
	}

	/// Returns the impact position of a raycast on the ground, based on mouse position.!-- Returns true if it hit the ground and got a valid position
	public bool RaycastMouseToGround (out Vector3 hitPoint) {
		int mask = LayerMask.NameToLayer ("DirtFloor");
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		RaycastHit hit;

		if (Physics.Raycast (ray, out hit, Mathf.Infinity, 1 << mask)) {
			hitPoint = hit.point;
			return true;
		} else {
			hitPoint = Vector3.zero;
			return false;
		}
	}

	/// Like RaycastMouseToGround, but outputs a grid position
	public bool RaycastMouseToGrid (out Vector3 hitPoint) {
		if (RaycastMouseToGround(out hitPoint)) {
			return true;
		} else {
			return false;
		}
		
	}

	/// Returns true if the point is inside the grid
	public bool CheckInGridBounds (Vector2Int gridPos) {
		if (	gridPos.x == Mathf.Clamp(gridPos.x, 0, gridWidth - 1) 
			&& 	gridPos.y == Mathf.Clamp(gridPos.y, 0, gridHeight - 1)) {
				return true;
			} else {
				return false;
			}
	}
}
