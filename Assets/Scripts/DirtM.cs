﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class DirtM : MonoBehaviour {

	public abstract DirtType DirtType {get;}
	public int id;
	public Vector2Int gridPos;
	public GameObject pendingEffectObject;

	private static int idCounter = 0;
	public static int GetNextID () {
		return ++idCounter;
	}

	private bool hasGridPos = false;

	public virtual void Awake () {
		id = GetNextID ();
	}

	public void SetPendingEffect (bool isPending) {
		pendingEffectObject.SetActive (isPending);
	}

	/// Returns this piece of dirt's position in the grid
	/*private Vector2Int FindGridPosition () {
		for (int x = 0; x < DirtManager.Instance.gridWidth; x++) {
			for (int y = 0; y < DirtManager.Instance.gridHeight; y++) {
				gridPos = new Vector2Int (x, y);
				//if (DirtManager.Instance.GetDirtObect(gridPos) == null) continue;
				if (DirtManager.Instance.GetDirtObect(gridPos).GetComponent<DirtM>().id == id) {
					return gridPos;
				}
			}
		}
		Debug.LogError ("Could not find this dirt's position!");
		return new Vector2Int(0,0);

	}*/


}
