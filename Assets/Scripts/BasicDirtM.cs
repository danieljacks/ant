﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicDirtM : DirtM {

	public override DirtType DirtType {
		get {
			return DirtType.Basic;
		}
	}

}
